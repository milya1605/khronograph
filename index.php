<?
$page = "index";
include "/config.php";
?>
<!DOCTYPE html>
<html lang="ru-RU">
<head>
	<meta charset="UTF-8">
	<title><?=$arrayPages['title'];?></title>
	<meta name="description" content="<?=$arrayPages['description'];?>">
	<meta name="keywords" content="<?=$arrayPages['keywords'];?>">
	<link rel="stylesheet" href="css/style.css">
</head>
<body>
	<div class="header">
		<div class="logo">
			<a href="/"><img src="images/logo.png" alt=""></a>
		</div>

		<div class="tel">
			+7 (4832) 66-54-36
		</div>
	</div>
		
	<div class="menu">
		<ul>
			<li><a href="/">Главная</a></li>
			<li><a href="/catalog.php">Каталог</a></li>
			<li><a href="/news.php">Новости</a></li>
			<li><a href="/gb.php">Отзывы</a></li>
			<li><a href="/gallery.php">Галерея</a></li>
			<li><a href="/contacts.php">Контакты</a></li>
		</ul>
	</div>

	<div class="pages">
		<div class="content">
			<h1><?=$arrayPages['header'];?></h1>
			<?=$arrayPages['text'];?>
		</div>

		<div class="sidebar">
			<?
			include "/block_news.php";
			?>
		</div>

		<div class="foot"></div>
	</div>

	<div class="footer">
		<div class="copy">
			2016 &copy; "Хронограф"
		</div>
		<div class="info">
			г.Брянск, ул. Красноармейская, д.123 <br>
			+7 (4832) 66-54-36
		</div>
	</div>

</body>
</html>