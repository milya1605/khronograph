<?
$page = "otzyvy";
include "/config.php";

if (isset($_POST['submit'])) {
	$name = $_POST['name'];
	$email = $_POST['email'];
	$text = $_POST['text'];

	$result = addOtzyv($name, $email, $text);
	if ($result) {
		$_SESSION['msg'] = "<div class='success'>Ваш отзыв успешно отправлен</div>";
		header("Location: gb.php");
		exit();
	}else{
		$_SESSION['msg'] = "<div class='warning'>Ваш отзыв не удалось добавить. Попробуйте позже.</div>";
		header("Location: gb.php");
		exit();
	}
}

?>
<!DOCTYPE html>
<html lang="ru-RU">
<head>
	<meta charset="UTF-8">
	<title><?=$arrayPages['title'];?></title>
	<meta name="description" content="<?=$arrayPages['description'];?>">
	<meta name="keywords" content="<?=$arrayPages['keywords'];?>">
	<link rel="stylesheet" href="css/style.css">
</head>
<body>
	<div class="header">
		<div class="logo">
			<a href="/"><img src="images/logo.png" alt=""></a>
		</div>

		<div class="tel">
			+7 (4832) 66-54-36
		</div>
	</div>

	<div class="menu">
		<ul>
			<li><a href="/">Главная</a></li>
			<li><a href="/catalog.php">Каталог</a></li>
			<li><a href="/news.php">Новости</a></li>
			<li><a href="/gb.php">Отзывы</a></li>
			<li><a href="/gallery.php">Галерея</a></li>
			<li><a href="/contacts.php">Контакты</a></li>
		</ul>
	</div>

	<div class="pages">
		<div class="content">
			<h1><?=$arrayPages['header'];?></h1>
			<?=$arrayPages['text'];?>
			<div class="otzyvy">
				<div class="msg">
					<?=$_SESSION['msg'];?>
				</div>
				<div class="form">
					<form action="" method="post">
						<p>
							<label for="name">Ваше имя:</label>
							<input type="text" id="name" name="name">
						</p>
						<p>
							<label for="email">Ваш Email:</label>
							<input type="text" id="email" name="email">
						</p>
						<p>
							<label for="text">Ваш отзыв:</label>
							<textarea name="text" id="text" cols="30" rows="10"></textarea>
						</p>
						<p>
							<input type="submit" name="submit" value="Отправить">
						</p>
					</form>

					
				</div>
				<div class="list-otzyv">
					<table border="0">
						<? foreach($arrayOtzyvy as $otzyv):?>
						<tr>
							<td class="name-user"><?=$otzyv['id'];?> . <?=$otzyv['name_user'];?></td>
							<td class="email-user"><?=$otzyv['email_user'];?></td>
						</tr>
						<tr>
							<td class="text-user" colspan="2">
								<?=$otzyv['text'];?>
							</td>
						</tr>
						<? endforeach;?>
					</table>
				</div>
			</div>
		</div>

		<div class="sidebar">
			<?
			include "/block_news.php";
			?>
		</div>

		<div class="foot"></div>
	</div>

	<div class="footer">
		<div class="copy">
			2016 &copy; "Хронограф"
		</div>
		<div class="info">
			г.Брянск, ул. Красноармейская, д.123 <br>
			+7 (4832) 66-54-36
		</div>
	</div>

</body>
</html>
<?
unset($_SESSION['msg']);
?>